import random
import time

# Greeting
print("Greetings"); time.sleep(1.25)
print("Welcome to Annie's magic number guessing game.\n"); time.sleep(1.25)

# Rules
print("Rules:"); time.sleep(1)
print("    1. Pick a number between 1 and 100"); time.sleep(1.12)
print("    2. The computer will say if you are too high or too low or correct"); time.sleep(2)
print("    3. Keep guessing until you are right."); time.sleep(1.15)

# Initialise response
response = "Y"

# Start the loop
while response == "Y" or response == "YES":

    # Random Number
    answer = random.randint(1, 101)

    # User guess
    guess = input("\nPlease pick a number: ")
    score = 0

    # Check numbers
    while guess:
        score += 1
        
        if guess < answer :
            print("Too low.")
        
        if guess > answer:
            print("Too high.")
        
        guess = input("\nGuess again: ")

        if guess == answer:
            print("\nCongratulations!!!")
            print("You won the game. Good job.\n"); time.sleep(0.75)
            print("Your score was: " + str(score))
            break
        
    # Play again
    response = raw_input("Do you want to play again? Y/N\n")
    response = response.upper()
    
    # Exit game
    if response == "N" or response == "NO":
        break
    
print("\nThanks for playing.")
