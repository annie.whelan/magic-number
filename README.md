# Guess the magic number Python game.

To play this game simply run the script in a terminal using the following command:

`python magic_number.py`


<img src="images/screenshot.png"  width="513" height="378">

Example of the game running in an Xterm window.


### To do:
- [x] Remove redundant `if` statement outside `while` loop
- [x] Add "play again" functionality
- [x] Add time delay for better reading of `print` items
- [ ] Enable error handling
- [x] Add scoring
- [ ] Add leaderboard
- [ ] Add function to display rules

